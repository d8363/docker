FROM --platform=linux/amd64 gradle:alpine

WORKDIR /app

RUN git clone https://gitlab.com/d8363/backend.git

WORKDIR /app/backend

RUN gradle clean build -x test --no-daemon -i --stacktrace

FROM --platform=linux/amd64 openjdk:17-alpine

RUN mkdir -p /app

WORKDIR /app

COPY --from=0 app/backend/build/libs/*.jar ./app.jar

EXPOSE $PORT

CMD [ "java", "-jar", "./app.jar" ]